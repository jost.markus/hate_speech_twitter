## Code for "Die Entwicklung von Hate Speech auf Twitter unter Elon Musk"

### Usage
The code consists of three parts:
* Collecting tweets ([scrape_tweets.ipynb](https://gitlab.ub.uni-bielefeld.de/jost.markus/hate_speech_twitter/-/blob/main/scrape_tweets.ipynb))
* Classifying tweets ([classify_tweets.ipynb](https://gitlab.ub.uni-bielefeld.de/jost.markus/hate_speech_twitter/-/blob/main/classify_tweets.ipynb))
* Plotting results ([data_analyis.ipynb](https://gitlab.ub.uni-bielefeld.de/jost.markus/hate_speech_twitter/-/blob/main/data_analysis.ipynb))

The scraped and labeled tweets can be found in the [data](https://gitlab.ub.uni-bielefeld.de/jost.markus/hate_speech_twitter/-/tree/main/data) folder.
